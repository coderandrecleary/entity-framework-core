using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System;

namespace EFGetStarted

{
    public class EFGetStarted
    {
        public static void Main(String[] args)
        {

            Author author = new Author();

            author.FirstName = "John";
            // author.FirstName.set("John");

            var name = author.FirstName;
            //var name =  author.FirstName.get();
            author.LastName = "Mark";


        }

    }

    public class BloggingContext : DbContext
    {

        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }

        public DbSet<Author> Authors { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite("Data Source=blogging.db");
    }

    public class Blog
    {
        public int BlogId { get; set; }
        public string Url { get; set; }

        public List<Post> Posts { get; } = new List<Post>();

        public int AuthorId { get; set; }
        public Author Author { get; set; }

    }

    public class Post
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public int BlogId { get; set; }
        public Blog Blog { get; set; }
    }



    public class Author
    {
        public int Id { get; set; }
        private string _firstName;
        public string FirstName
        {
            get
            {
                return this._firstName;
            }

            set
            {
                this._firstName = value;
            }
        }

        public string LastName { get; set; }
        public int Age { get; set; }

        public List<Blog> Blog = new List<Blog>();

    }

}
